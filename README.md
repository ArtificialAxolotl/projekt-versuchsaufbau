# Projektarbeit Bachelor FB04 an der Hochschule Niederrhein
Ziel der Projektarbeit war es in Kooperation mit fünf weiteren Studenten eine Prüfstation zu entwickeln welche das Relaxations- und Kriechverhalten von faservertärkten 3D gedruckten Kunststofffedern testet.

## Motivation
Datenerfassung über das Kriech- und Relaxationsverhalten faserverstärkter Kunststofffedern. Zurzeit gibt es solche Daten nur für Federn aus Metall.

## Ziel
Entwurf eines Versuchsaufbaus, um Messwerte verschiedener Federgeometrien von Kunststofffedern zu generieren.

## Ergebnis
- Prüfanlage zur Untersuchung des Kriech- und Relaxationsverhaltens faserverstärkter Kunststofffedern 
- Ohne Umbau der Prüfanlage
- Automatisch generiertes Messprotokoll

### Technische Daten:
- Abmaße: 650 x 300 x 250 mm³
- Max. Federgeometrie: 170 x 140 x 170 mm³
- Max. Prüfkraft: 700 N
- Wegmessgenauigkeit: ± 0,01 mm

## Fazit
Das Bachelorprojekt konnte erfolgreich abgeschlossen werden. Wir haben neue Erfahrungen in Themen wie Projektmanagement und Organisation sowie individueller Aufgabenverteilung erhalten. Außerdem konnte das Zusammenspiel und die Abhängigkeiten elektronischer und mechanischer Komponenten erfasst werden.


