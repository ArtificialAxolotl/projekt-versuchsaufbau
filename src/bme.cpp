#include <SparkFunBME280.h>

BME280 bme; // Uses default I2C address 0x76

/*
  VCC =  1.7 to 3.6V.  Connect to 3.3V output of the MCU
  GND =  Ground
  SCL =  Clock (SCL / SCK) for I2C and SPI
  SDA =  Data (SDA / SDI) for I2C and SPI
  CSB =  Chip Select Bus.  Logic HIGH for I2C (default), logic LOW for SPI
  SDO =  Data Out (SDO) for SPI.  Sets I2C address for I2C
*/

int initBME280() {
  bme.setI2CAddress(0x76);

  if (!bme.beginI2C()) {
    return -1;
  }
  return 1;
}

float readTemp() { return bme.readTempC(); }

float readHum() { return bme.readFloatHumidity(); }
