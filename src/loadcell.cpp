#include <HX711_ADC.h>

// pins:
const int HX711_dout = 32; // mcu > HX711 dout pin 4
const int HX711_sck = 33;  // mcu > HX711 sck pin 2

#define sample_rate 128 //16, 32
#define DEBUG_LOADCELL

// HX711 constructor:
HX711_ADC LoadCell(HX711_dout, HX711_sck);

int initLoadcell() {
  LoadCell.begin();
  LoadCell.setSamplesInUse(sample_rate);

  // uncomment this if you want to set the calibration value in the sketch
  float calibrationValue = -45.23; //-44.69
  long stabilizingtime = 3000;

  bool tare = true;
  //LoadCell.tare();
  LoadCell.start(stabilizingtime, tare);
  if (LoadCell.getTareTimeoutFlag()) {
    #ifdef DEBUG_LOADCELL
     Serial.println("Timeout, check MCU>HX711 wiring and pin designations");
     #endif
    return -1;
  } else {
    #ifdef DEBUG_LOADCELL
     Serial.print("calibrationfactor set to: ");
     Serial.println(calibrationValue);
     #endif
    LoadCell.setCalFactor(calibrationValue);
  }

  return 1;
}

float readloadcell() {
  static bool newDataReady = false;
  
  if (LoadCell.update())
    newDataReady = true;

  if (newDataReady) {
    return LoadCell.getData();
  }

  return -1;
}

float readloadcellfast() {
  LoadCell.setSamplesInUse(1);
  return readloadcell();
  LoadCell.setSamplesInUse(sample_rate);
}

float getForce() {
  float loadcellVal = readloadcell();
  if (loadcellVal != -1) {
    float force = (loadcellVal / 1000) * 9.81; // F = m * g
    return force;
  }
  return 0;
}

void tare(){
  LoadCell.tare();
}
// loadcell calibration
// --------------------------------------------------------------------------------------

void loadcellCalibration() {
  //LoadCell.setSamplesInUse(128);

  Serial.println("***");
  Serial.println("Start calibration:");
  Serial.println("Place the load cell an a level stable surface.");
  Serial.println("Remove any load applied to the load cell.");
  Serial.println("Send 't' from serial monitor to set the tare offset.");

  bool resume = false;
  while (resume == false) {
    LoadCell.update();
    if (Serial.available() > 0) {
      char inByte = Serial.read();
      if (inByte == 't')
        LoadCell.tareNoDelay();
    }
    if (LoadCell.getTareStatus() == true) {
      Serial.println("Tare complete");
      resume = true;
    }
  }

  Serial.println("Now, place your known mass on the loadcell.");
  Serial.println(
      "Then send the weight of this mass (i.e. 100.0) from serial monitor.");

  float known_mass = 0;
  resume = false;
  while (resume == false) {
    LoadCell.update();
    if (Serial.available() > 0) {
      known_mass = Serial.parseFloat();
      if (known_mass != 0) {
        Serial.print("Known mass is: ");
        Serial.println(known_mass);
        resume = true;
      }
    }
  }

  LoadCell.refreshDataSet(); // refresh the dataset to be sure that the known
                             // mass is measured correct
  float newCalibrationValue =
      LoadCell.getNewCalibration(known_mass); // get the new calibration value

  Serial.print("New calibration value has been set to: ");
  Serial.print(newCalibrationValue);
  Serial.println(
      ", use this as calibration value (calFactor) in your project sketch.");

  Serial.println("End calibration");
  Serial.println("***");
  Serial.println("To re-calibrate, send 'r' from serial monitor.");
  Serial.println("For manual edit of the calibration value, send 'c' from "
                 "serial monitor.");
  Serial.println("***");
}