#include <Arduino.h>

#include "bme.h"
#include "communication.h"
#include "loadcell.h"
#include "main.h"
#include "motion.h"
#include "poti.h"

#define DEBUG_CREEP

void startCreepAttempt(parameter param) {
  struct measurements mes;

  // Timer for sending measurements
  const unsigned int sendInterval = 5000;
  unsigned long startTimer = 0;
  unsigned long endTimer = 0;

  // start values
  float startForce;
  double startDistance;

  // approach the feather
  approach(param.height);
#ifdef DEBUG_CREEP
  Serial.println("approach complete.");
  delay(2000);
#endif

  // POTI reset (top of feather)
  startDistance = readPotiSingle();
#ifdef DEBUG_CREEP
  Serial.print("Poti starting value: ");
  Serial.println(startDistance);
  delay(2000);
#endif

  // adjust force -------------
  while (getForce() < param.force) {
#ifdef DEBUG_CREEP
    Serial.print(getForce());
    Serial.print(" / ");
    Serial.println(param.force);
#endif
    // TODO: finetuning -> smaller steps, measure longer
    if((param.force - getForce()) > 2.0){ 
      // move 1mm down
      move(1.0);
      delay(1000);
    }
  }
  stop(); //deactivate Driver
  startForce = getForce();
#ifdef DEBUG_CREEP
  Serial.print("Force starting value: ");
  Serial.println(startForce);
  delay(2000);
#endif

  // measure runtime
  long startTime = millis();
  mes.runtime = millis() - startTime;

  // START-----------------------------------------------------------------------------------
  while (mes.runtime < param.duration) {

    startTimer = millis();
    if (startTimer - endTimer > sendInterval) {
      mes.temperature = readTemp();                // Temperatur
      mes.humidity = readHum();                    // Luftfeuchtigkeit
      mes.distance = (readPotiSingle() - startDistance); // TEST Konpressionsdistanz
      delay(10);
      //writeSerial(mes);
      writeSerial1(mes);
      endTimer = startTimer;
    }
    mes.force = getForce(); // 20N , sobald <20 N -> nachregeln auf 20N
    
    // TODO: deactivate Motor ?
    //readjustment - P-Regler
    if ((startForce - mes.force) >= 1.0) { // 20N - 19N = 1
      move(0.1);
    } else if ((startForce - mes.force) < -1.0) { // 20N - 21N = -1
      move(-0.1);
    }
    

    mes.mode = 1; // stay in creep attempt mode
    mes.runtime = millis() - startTime;
  }
  // END-----------------------------------------------------------------------------------

  mes.mode = 0; // go into idle mode
  mes.temperature = NAN;
  mes.humidity = NAN;
  mes.distance = NAN;
  mes.force = NAN;

  // Serial.print("mode: ");Serial.println(mes.mode);
  //writeSerial(mes);
  writeSerial1(mes);
  delay(10);
}
