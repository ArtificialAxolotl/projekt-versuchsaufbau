#include <Arduino.h>

#include "bme.h"
#include "communication.h"
#include "loadcell.h"
#include "main.h"
#include "poti.h"
#include "motion.h"

#define DEBUG_RELAX


void startRelaxationAttempt(parameter param) {
  struct measurements mes;
  // Timer for sending measurements
  const unsigned int sendInterval = 5000;
  unsigned long startTimer = 0;
  unsigned long endTimer = 0;

  // start values
  float startForce;
  double potiStart;

  // approach the feather
  approach(param.height);

  //TEST: POTI reset (top of feather)
  potiStart = readPotiSingle();

  // measure runtime
  long startTime = millis();
  mes.runtime = millis() - startTime;

  // START-----------------------------------------------------------------------------------
  while (mes.runtime < param.duration) {

    startTimer = millis();
    if (startTimer - endTimer > sendInterval) {
      mes.temperature = readTemp(); // Temperatur
      mes.humidity = readHum();     // Luftfeuchtigkeit

      //TEST: DISTANZ-Differenz messen
      mes.distance = readPotiSingle() - potiStart; // TEST Konpressionsdistanz

      writeSerial(mes);
      endTimer = startTimer;
    }
    mes.force = getForce();

    mes.mode = 2; // stay in relax attempt mode
    mes.runtime = millis() - startTime;
  }
  // END-----------------------------------------------------------------------------------

  mes.mode = 0; // go into idle mode
  mes.temperature = NAN;
  mes.humidity = NAN;
  mes.distance = NAN;
  mes.force = NAN;

#ifdef DEBUG_RELAX
  Serial.print("mode: ");Serial.println(mes.mode);
#endif
  writeSerial(mes);
}
