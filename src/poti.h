int initPoti();

//CONTINUOUS---------------------------------------------------------------------------
int readPotiRaw();

double readPoti();

//SINGLE--------------------------------------------------------------------------------
int readPotiRawSingle();

double readPotiSingle();

//Linear Regression---------------------------------------------------------------------
double readPotiLR();

int onChangePoti();