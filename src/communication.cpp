#include <ArduinoJson.h>

#include "main.h"

//#define DEBUG_SERIAL

StaticJsonDocument<200> in;
StaticJsonDocument<200> out;

// test
char json[] = "{\"mode\": 0,\"duration\":34000,\"distance\": 90.87, \"force\":6.11}";
// {"mode":1,"duration":60000,"distance":10.00,"force":20,"height":90, "mm":-10}

  unsigned long startT = 0;
  unsigned long endT = 0;

//TEST
void writeReady(){
  struct measurements mes;
  mes.mode = 0; // idle mode
  //mes.runtime = NAN;
  mes.temperature = NAN;
  mes.humidity = NAN;
  mes.distance = NAN;
  mes.force = NAN;

  out["mode"] = mes.mode;
  out["runtime"] = mes.runtime;
  out["distance"] = mes.distance;
  out["force"] = mes.force;
  out["temperature"] = mes.temperature;
  out["humidity"] = mes.humidity;

  // generate json and send to Serial
  serializeJson(out, Serial1);
  Serial1.println();

#ifdef DEBUG_SERIAL
  serializeJson(out, Serial);
  Serial.println();
#endif
}

parameter readSerial1Ready() {
  struct parameter param;
  const unsigned int interval = 10000;

  startT = millis();
  if (startT - endT > interval) {
    writeReady();
    endT = startT;
  }
  // receive Data vom Serial
  String buffer;
  //while (Serial.available() == 0) {} // waiting for Serial input
  if (Serial1.available()){
    buffer = Serial1.readStringUntil('\n');

  #ifdef DEBUG_SERIAL
      Serial.println(buffer);
  #endif

    // deserialize without errorhandling
    deserializeJson(in, buffer);

    // write Data to struct 'parameter'
    param.mode = in["mode"];
    param.duration = in["duration"];
    param.distance = in["distance"];
    param.force = in["force"];
    param.height = in["height"];
    param.mm = in["mm"];

    return param;
  }
}


// Fertig
parameter readSerial1() {
  struct parameter param;
  // receive Data vom Serial1 (RX:9, TX:10)
  String buffer;
  while (Serial1.available() == 0) {} // waiting for Serial 

  buffer = Serial1.readStringUntil('\n');

  #ifdef DEBUG_SERIAL
      Serial.println(buffer);
  #endif

  // deserialize without errorhandling
  deserializeJson(in, buffer);

  // write Data to struct 'parameter'
  param.mode = in["mode"];
  param.duration = in["duration"];
  param.distance = in["distance"];
  param.force = in["force"];
  param.height = in["height"];
  param.mm = in["mm"];

  return param;
}

void writeSerial1(measurements mes) {
  out["mode"] = mes.mode;
  out["runtime"] = mes.runtime;
  out["distance"] = mes.distance;
  out["force"] = mes.force;
  out["temperature"] = mes.temperature;
  out["humidity"] = mes.humidity;

  // generate json and send to Serial
  serializeJson(out, Serial1);
  Serial1.println();

#ifdef DEBUG_SERIAL
  serializeJson(out, Serial);
  Serial.println();
#endif
}

parameter readSerial() {
  struct parameter param;

  // receive Data vom Serial
  String buffer;
  while (Serial.available() == 0) {} // waiting for Serial input

  buffer = Serial.readStringUntil('\n');

  #ifdef DEBUG_SERIAL
      Serial.println(buffer);
  #endif

  // deserialize without errorhandling
  deserializeJson(in, buffer);

  // write Data to struct 'parameter'
  param.mode = in["mode"];
  param.duration = in["duration"];
  param.distance = in["distance"];
  param.force = in["force"];
  param.height = in["height"];
  param.mm = in["mm"];

  return param;
}

void writeSerial(measurements mes) {
  out["mode"] = mes.mode;
  out["runtime"] = mes.runtime;
  out["distance"] = mes.distance;
  out["force"] = mes.force;
  out["temperature"] = mes.temperature;
  out["humidity"] = mes.humidity;

  // generate json and send to Serial
  serializeJson(out, Serial);
  Serial.println();
}
