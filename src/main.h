#include <Arduino.h>

#ifndef MAIN_H
#define MAIN_H

// get
struct parameter {
  uint mode = 0;
  uint duration = 0;
  float distance = NAN; //-> compression for relaxation
  float force = NAN;    // creep attempt
  uint height = 0;    // Springheight
  // motor
  int mm = 0; // 1, 10, 100 mm
};

// set
struct measurements {
  uint mode;
  uint runtime = 0;
  double distance = NAN;
  float force = NAN;
  float temperature = NAN;
  float humidity = NAN;
};

#endif