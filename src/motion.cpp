#include "loadcell.h"
#include <Arduino.h>
#include <TMCStepper.h>

/*
  NEMA23 Stepper L5918S2008-T10x2 - Two stepper parallel
  TMC5160
*/

#define DEBUG_TMC

#define EN_PIN 17   // enable
#define CS_PIN 5    // Chip select //SS slave select
#define MOSI_PIN 23 // Software Master Out Slave In (MOSI)
#define MISO_PIN 19 // Software Master In Slave Out (MISO)
#define SCK_PIN 18  // Software Slave Clock (SCK)

#define R_SENSE 0.075f // TMC5160: 0.075 Ohm

TMC5160Stepper tmc =
    TMC5160Stepper(CS_PIN, R_SENSE, MOSI_PIN, MISO_PIN, SCK_PIN);

// Bauraum
//- UP
//+ DOWN
int Zmax = 210;           // 20cm = 200 mm
int RPS = 51200;          // 1 RPS = 256(usteps)*200(steps/round)
int steps_per_mm = 25600; // 1/256 steps

int approach_gap = 5; // 5 mm

void initMotor() {
  pinMode(CS_PIN, OUTPUT);
  pinMode(EN_PIN, OUTPUT);
  digitalWrite(CS_PIN, HIGH);
  digitalWrite(EN_PIN, HIGH); // deactivate driver

  pinMode(MOSI_PIN, OUTPUT);
  pinMode(MISO_PIN, INPUT);
  pinMode(SCK_PIN, OUTPUT);

  // set driver config
  tmc.begin();

  // standard config
  tmc.rms_current(600); // RMS current
  tmc.toff(4);          // Sets the slow decay time; 0 for freewheel
  tmc.blank_time(24);   // sets-> tmc.tbl(); 16,24,36,54

  // Activation of StealthChop also disables DcStep.
  // ------------------------------------------------------------
  tmc.en_pwm_mode(1); // StealthChop voltage PWM mode enabled
  // current settings
  // --------------------------------------------------------------
  tmc.irun(14);  // Motor run current 0-31
  tmc.ihold(10); // Standstill current 0-31
  // number of clock cycles for motor power down
  tmc.iholddelay(5);
  // sets the delay time after stand still 0-4 ; 10: Reset Default
  tmc.TPOWERDOWN(2);

  /*
    // StallGuard config (without DcStep)
    tmc.sgt(0);   //controls the StallGuard2 threshold level for stall detection
    tmc.sfilt(0); //Enables the StallGuard2 filter for more precision; 0:
    standard, 1: filtered tmc.TCOOLTHRS(0xFFFFF); tmc.THIGH(0);  //Upper
    velocity threshold
    //tmc.sg_result(); // StallGuard2 result; high: less load, 0: stall
    //1. const. velocity
    //2. apply load, stall before sg_result = 0 : SGT--,  no stall at sg_result
    = 0 : SGT++,
    //3. TCOOLTHRS() > TSTEP() -> enable stop on stall with sg_stop(1); restart
    Motor with sg_stop(0) or RAMP_STAT
    //CoolStep config
    -----------------------------------------------------------------------
    tmc.semin(5);  //0-15; set the lower threshold
    tmc.semax(2);  //0-15; controls an upper threshold
    tmc.sedn(0b01);  //Sets the number of StallGuard2 readings above the upper
    threshold
    //tmc.sg_stop(1);
  */

  /*
  // DcStep config
  -------------------------------------------------------------------
  tmc.VDCMIN();       //
  tmc.dc_time(25);    // tbl + 1..100;

  // init StallGuard2 with DcStep
  --------------------------------------------------- tmc.dc_sg();     //
  slightly higher than DC_TIME / 16; controls stall detection in DcStep
  tmc.TCOOLTHRS(); // Set TCOOLTHRS to match TSTEP at a velocity slightly above
  VDCMIN for lower stallGuard velocity limit
  // tmc.TSTEP(); //Actual measured time between two 1/256 microsteps
  tmc.sg_stop(1); // Enable to stop the motor upon stall detection
  */
  // tmc.RAMP_STAT();     // Read out RAMP_STAT to clear event_stop_sg and
  // restart the motor
}

int mm_to_steps(float mm) { return (mm * steps_per_mm); }

bool move(float movement) {

  digitalWrite(EN_PIN, LOW); // activate driver

  tmc.VSTART(0);   // start velocity
  tmc.A1(2000);    // acceleration till V1 is reached
  tmc.V1(10000);   // velocity for A1,D1
  tmc.D1(3000);    // deceleration from V1 to VSTOP; D1 >= A1
  tmc.AMAX(3000);  // acceleration till VMAX is reached; AMAX < A1
  tmc.VMAX(20000); // velocity maximal slow:20000 fast:60000
  tmc.DMAX(2000);  // deceleration from VMAX to V1; DMAX >= AMAX
  tmc.VSTOP(10);

  tmc.RAMPMODE(0); // 0: positioning mode; 3: hold mode

  tmc.XTARGET(tmc.XACTUAL() + mm_to_steps(movement));
  delay(100);
  while (tmc.VACTUAL() != 0) {
    delay(100);
  }
  if (tmc.XACTUAL() == tmc.XTARGET()) {
    return 1;
  }

  return 0;
}

bool homing() {

  tmc.VSTART(0);   // start velocity
  tmc.A1(4000);    // acceleration till V1 is reached
  tmc.V1(30000);   // velocity for A1,D1
  tmc.D1(3000);    // deceleration from V1 to VSTOP; D1 >= A1
  tmc.AMAX(3000);  // acceleration till VMAX is reached; AMAX < A1
  tmc.VMAX(60000); // velocity maximal slow:20000 fast:60000
  tmc.DMAX(3000);  // deceleration from VMAX to V1; DMAX >= AMAX
  tmc.VSTOP(10);
  tmc.RAMPMODE(0); // with AMAX to VMAX

  // tmc.sg_stop(1);  // activate stop on stall

  // TODO: This needs an Endstop (either HW or StallGuard)

  tmc.XTARGET(mm_to_steps(-Zmax)); // Zmax
  // FIXME: This doesn't work in the Library
  // Serial.println(tmc.position_reached());

  // delay until target reached
  while (tmc.XACTUAL() != tmc.XTARGET()) {
    delay(100);
  }

  // acertain target reached
  if (tmc.XTARGET() == tmc.XACTUAL()) {

    tmc.XACTUAL(0);
    tmc.XTARGET(0);

#ifdef DEBUG_TMC
    Serial.print("Actual Position NOW: ");
    Serial.println(tmc.XACTUAL());
#endif

    return 1;
  }
  /*
    //crash detected?
    if(tmc.stallguard()){ //tmc.stallguard() / status_sg()
      tmc.XACTUAL(Zmax); //setting home position

      //tmc.sg_stop(0);    //reset motor

      tmc.XTARGET(mm_to_steps(50)); //move 10 mm down

      return 1;
    }
    */
  return 0;
}

bool homing_endstopSwitch() {
  tmc.en_softstop(1); // softstop enable
  // Move UP (-) -> left switch  Pin:REFL
  tmc.pol_stop_l(1);    // low level stops the motor
  tmc.stop_l_enable(1); // activate interrupt
  // Activates latching of the position to XLATCH
  tmc.latch_l_inactive(1);

  digitalWrite(EN_PIN, LOW); // activate driver

  tmc.VSTART(0);   // start velocity
  tmc.A1(4000);    // acceleration till V1 is reached
  tmc.V1(30000);   // velocity for A1,D1
  tmc.D1(3000);    // deceleration from V1 to VSTOP; D1 >= A1
  tmc.AMAX(3000);  // acceleration till VMAX is reached; AMAX < A1
  tmc.VMAX(40000); // velocity maximal slow:20000 fast:60000
  tmc.DMAX(3000);  // deceleration from VMAX to V1; DMAX >= AMAX
  tmc.VSTOP(10);
  tmc.RAMPMODE(0); // with AMAX to VMAX

  tmc.XTARGET(-mm_to_steps(Zmax));
  delay(100);

#ifdef DEBUG_TMC
  // Serial.print(tmc.XACTUAL());
  // Serial.print(" / ");
  Serial.println(tmc.XTARGET());
#endif
  while (tmc.VACTUAL() != 0) {
    delay(100);
  }

  if (tmc.VACTUAL() == 0) {
#ifdef DEBUG_TMC
    Serial.println("Standstill. Starting Hold Mode.");
#endif
    tmc.RAMPMODE(3); // hold mode
    delay(10);
    // set home position and wait
    tmc.XACTUAL(0);
    tmc.XTARGET(0);
    delay(500);

#ifdef DEBUG_TMC
    Serial.print("xactual NOW: ");
    Serial.println(tmc.XACTUAL());
#endif

    // drive away from the switch
    tmc.VSTART(0);   // start velocity
    tmc.A1(4000);    // acceleration till V1 is reached
    tmc.V1(30000);   // velocity for A1,D1
    tmc.D1(3000);    // deceleration from V1 to VSTOP; D1 >= A1
    tmc.AMAX(3000);  // acceleration till VMAX is reached; AMAX < A1
    tmc.VMAX(20000); // velocity maximal slow:20000 fast:60000
    tmc.DMAX(3000);  // deceleration from VMAX to V1; DMAX >= AMAX
    tmc.VSTOP(10);
    tmc.RAMPMODE(0); // with AMAX to VMAX

    tmc.XTARGET(mm_to_steps(10));
    delay(100);
    while (tmc.XACTUAL() != tmc.XTARGET()) {
      delay(100);
    }
    digitalWrite(EN_PIN, HIGH); // deactivate driver
    return 1;
  }
  return 0;
}

void approach(int height) {
  int loadThrs1 = 20;
  int loadThrs2 = 10;
  int loadThrs3 = 5;

  height = height + approach_gap;

  int loadOLD = (int)readloadcellfast();

#ifdef DEBUG_TMC
  Serial.println("approach --------------------------------");
  Serial.print("height:");
  Serial.println(height);
  Serial.print("load:");
  Serial.println(loadOLD);
  delay(3000);
#endif

  digitalWrite(EN_PIN, LOW); // activate driver

  // fast approach
  tmc.VSTART(0);  // start velocity
  tmc.A1(3000);   // acceleration till V1 is reached
  tmc.V1(25000);  // velocity for A1,D1
  tmc.D1(3000);   // deceleration from V1 to VSTOP; D1 >= A1
  tmc.AMAX(3000); // acceleration till VMAX is reached; AMAX < A1
  tmc.VMAX(RPS);  // velocity maximal slow:20000 fast:60000
  tmc.DMAX(3000); // deceleration from VMAX to V1; DMAX >= AMAX
  tmc.VSTOP(10);
  tmc.RAMPMODE(0); // with AMAX to VMAX

  tmc.XTARGET(mm_to_steps(Zmax - height));

#ifdef DEBUG_TMC
  // Serial.print(tmc.XACTUAL());
  // Serial.print(" / ");
  Serial.println(tmc.XTARGET());
#endif

  while (tmc.XACTUAL() != tmc.XTARGET()) {
    delay(100);
  }

  // slow approach
  // 60g aktuell - 60g anfangs = 0 | 80 - 60 = 10
  while (((int)readloadcellfast() - loadOLD) <= loadThrs1) {
    move(1.0); 
  }
  move(-1.0);
  while (((int)readloadcellfast() - loadOLD) <= loadThrs2) {
    move(0.1);
  }
  move(-0.1);
  while (((int)readloadcellfast() - loadOLD) <= loadThrs3) {
    move(0.01);
  }
  move(-0.01);

  /* OLD
    // loadcell
    bool load_detected = 0;
    float loadMIN = 100.0;
    float loadMAX = 0.0;

      loadACTUAL = readloadcellfast();
  #ifdef DEBUG_TMC
      Serial.print("load:");
      Serial.println(loadOLD);
  #endif
      if (loadACTUAL > loadMAX) {
        loadMAX = loadACTUAL;
      }
      if (loadACTUAL < loadMIN) {
        loadMIN = loadACTUAL;
      }
      //TODO: finer approach
      move(1.0); // 1mm down
      delay(1000); //TODO: zu ungenau
  #ifdef DEBUG_TMC
      Serial.print("diff:");
      Serial.println(loadMAX - loadMIN);
  #endif
      if ((loadMAX - loadMIN) > loadThreshold {
        tmc.XTARGET(tmc.XACTUAL());
        delay(100);
        load_detected = 1;
      }

    }
    */
}

bool stop() {
  // check if driver is enabled | EN_PIN = LOW
  if (!tmc.drv_enn()) {
#ifdef DEBUG_TMC
    Serial.println("Driver is hardware enabled");
#endif

    digitalWrite(EN_PIN, HIGH); // deactivate Driver
    return 1;
  }

  return 0;
}

int pos = 0;

void testingMotor() {
  static uint32_t last_time = 0;
  uint32_t ms = millis();

  if ((ms - last_time) > 1000) // run every 1s
  {
    last_time = ms;

    if (tmc.diag0_error()) {
      Serial.println(F("DIAG0 error"));
    }
    if (tmc.ot()) {
      Serial.println(F("Overtemp."));
    }
    if (tmc.otpw()) {
      Serial.println(F("Overtemp. PW"));
    }
    if (tmc.s2ga()) {
      Serial.println(F("Short to Gnd A"));
    }
    if (tmc.s2gb()) {
      Serial.println(F("Short to Gnd B"));
    }
    if (tmc.ola()) {
      Serial.println(F("Open Load A"));
    }
    if (tmc.olb()) {
      Serial.println(F("Open Load B"));
    }
    if (tmc.stst()) {
      Serial.println("standstill indicator ");
    }
    if (tmc.stallguard()) {
      Serial.println("stallguard indicator ");
    }
  }

  tmc.VSTART(0);   // start velocity
  tmc.A1(4000);    // acceleration till V1 is reached
  tmc.V1(30000);   // velocity for A1,D1
  tmc.D1(3000);    // deceleration from V1 to VSTOP; D1 >= A1
  tmc.AMAX(3000);  // acceleration till VMAX is reached; AMAX < A1
  tmc.VMAX(40000); // velocity maximal slow:20000 fast:60000
  tmc.DMAX(3000);  // deceleration from VMAX to V1; DMAX >= AMAX
  tmc.VSTOP(10);
  tmc.RAMPMODE(0); // with AMAX to VMAX

  Serial.println("Type + or - for movement.");

  while (Serial.available() == 0) {
  }
  if (Serial.available()) {

    char buff = Serial.read();

    if (buff == '+') { // DOWN

      pos = pos + mm_to_steps(30);
      Serial.print("pos: ");
      Serial.println(pos);

      digitalWrite(EN_PIN, LOW);

      tmc.XTARGET(pos);
      delay(100);

      while (tmc.VACTUAL() != 0) {
        Serial.print(tmc.XACTUAL());
        Serial.print(" / ");
        Serial.println(tmc.XTARGET());
        delay(1000);
      }

    } else if (buff == '-') {
      // UP
      pos = pos - mm_to_steps(30);
      Serial.print("pos: ");
      Serial.println(pos);

      digitalWrite(EN_PIN, LOW); // activate driver

      tmc.XTARGET(pos);
      // tmc.XTARGET(-256000);
      delay(100);

      while (tmc.XTARGET() != tmc.XACTUAL()) {
        Serial.print(tmc.XACTUAL());
        Serial.print(" / ");
        Serial.println(tmc.XTARGET());
        delay(1000);
      }

    } else if (buff == 's') {
      // stop();
    } else if (buff == 'h') {
      homing_endstopSwitch();
    }
  }
}

void testingApproach() {

  tmc.VSTART(0);   // start velocity
  tmc.A1(2000);    // acceleration till V1 is reached
  tmc.V1(10000);   // velocity for A1,D1
  tmc.D1(2000);    // deceleration from V1 to VSTOP; D1 >= A1
  tmc.AMAX(2000);  // acceleration till VMAX is reached; AMAX < A1
  tmc.VMAX(20000); // velocity maximal slow:20000 fast:60000
  tmc.DMAX(2000);  // deceleration from VMAX to V1; DMAX >= AMAX
  tmc.VSTOP(10);
  tmc.RAMPMODE(0); // with AMAX to VMAX

  int initial_load = (int)readloadcell();
  Serial.print("Initial Loadcell value: ");
  Serial.println(initial_load);
  Serial.println("Type 1: 1.0mm, 2: -1.0mm, 3: 0.1mm, 4: -0.1mm,  5: above feather, 6: initial load");

  while (Serial.available() == 0) { }
  int buff = Serial.parseInt();
  Serial.println(buff);

  if (buff == 1) { // DOWN
    move(1.0);
    Serial.print("Delta: ");
    Serial.println((int)readloadcell() - initial_load);

    Serial.print("Delta fast: ");
    Serial.println((int)readloadcellfast() - initial_load);

  } else if (buff == 2) {
    move(-1.0);
    Serial.print("Delta: ");
    Serial.println((int)readloadcell() - initial_load);

    Serial.print("Delta fast: ");
    Serial.println((int)readloadcellfast() - initial_load);

  } else if (buff == 3) {
    move(0.1);
    Serial.print("Delta: ");
    Serial.println((int)readloadcell() - initial_load);

    Serial.print("Delta fast: ");
    Serial.println((int)readloadcellfast() - initial_load);

  } else if (buff == 4) {
    move(-0.1);
    Serial.print("Delta: ");
    Serial.println((int)readloadcell() - initial_load);

    Serial.print("Delta fast: ");
    Serial.println((int)readloadcellfast() - initial_load);
  }else if (buff == 5) {
      digitalWrite(EN_PIN, LOW);
      // fast approach
      tmc.VSTART(0);  // start velocity
      tmc.A1(3000);   // acceleration till V1 is reached
      tmc.V1(25000);  // velocity for A1,D1
      tmc.D1(3000);   // deceleration from V1 to VSTOP; D1 >= A1
      tmc.AMAX(3000); // acceleration till VMAX is reached; AMAX < A1
      tmc.VMAX(RPS);  // velocity maximal slow:20000 fast:60000
      tmc.DMAX(3000); // deceleration from VMAX to V1; DMAX >= AMAX
      tmc.VSTOP(10);
      tmc.RAMPMODE(0); // with AMAX to VMAX

      tmc.XTARGET(mm_to_steps(100));
  }
  else if (buff == 6){
    for (int i = 0; i < 256; i++){
      initial_load = (int)readloadcell();
      delay(10);
    }    
    Serial.print("Initial Loadcell value: ");
    Serial.println(initial_load);

  } else if (buff == 7) {
      homing_endstopSwitch();
  } else if (buff == 8){
      tare();
  }
}

/*
char buffer[256];
sprintf(buffer, "ioin=%#-10lx xactual=%7ld\n", tmc.IOIN(), xactual);
Serial.print(buffer);
*/