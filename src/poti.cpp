#include <ADS1115_WE.h>

ADS1115_WE adc = ADS1115_WE();

//-------------------------------------------------------------------------------
double MAX_RANGE_ANALOG = 25968.0; //3,3V ESP
double MIN_RANGE_ANALOG = 0.0;
double MIN_LENGHT = 0.00;
double MAX_LENGTH = 100.00;

//int startVal = 0;
double millimeter = 0.0;
int analogVal = 0;

// float scale_length = 96.7;
// float rectification = 0.058;

#define SINGLE

// Functions---------------------------------------------------------------------------

// Float interpolation function, for mapping the ADC reading
double dmap(double sensorValue, double sensorMin, double sensorMax, double outMin, double outMax) {
  return (sensorValue - sensorMin) * (outMax - outMin) / (sensorMax - sensorMin) + outMin;
}
float fmap(float sensorValue, float sensorMin, float sensorMax, float outMin, float outMax) {
  return (sensorValue - sensorMin) * (outMax - outMin) / (sensorMax - sensorMin) + outMin;
}

int initPoti() {
  // check if ads1115 is connected
  if (!adc.init()) {
    return -1;
  }
  // Init ADS1115
  //adc.setVoltageRange_mV(ADS1115_RANGE_6144); // Set the voltage range of the ADC to adjust the gain
  adc.setVoltageRange_mV(ADS1115_RANGE_4096);
  adc.setCompareChannels(ADS1115_COMP_0_GND); // Set the inputs to be compared -> compares 0 with GND
  adc.setConvRate(ADS1115_128_SPS); // Set the conversion rate in SPS (samples per second)
#if !defined SINGLE
  adc.setMeasureMode(ADS1115_CONTINUOUS); // Set continuous mode
#endif
#ifdef SINGLE
  adc.setMeasureMode(ADS1115_SINGLE); //Set single shot mode
#endif
  return 1;
}

//CONTINUOUS---------------------------------------------------------------------------
int readPotiRaw() {
 analogVal = adc.getRawResult(); // raw analog results
  return analogVal;
}

double readPoti() {
 analogVal = readPotiRaw();
  millimeter = dmap(analogVal, MIN_RANGE_ANALOG, MAX_RANGE_ANALOG, MAX_LENGTH, MIN_LENGHT); // Analog mappen in mm
  // millimeter = millimeter + correction;
  return millimeter;
}

//SINGLE--------------------------------------------------------------------------------
int readPotiRawSingle(){
  adc.startSingleMeasurement();
  while(adc.isBusy()){}
  int analogVal = adc.getRawResult();
  return analogVal;
}

double readPotiSingle() {
  analogVal = readPotiRawSingle();
  millimeter = dmap(analogVal, MIN_RANGE_ANALOG, MAX_RANGE_ANALOG, MAX_LENGTH, MIN_LENGHT);
  return millimeter;
}

// Linear Regression--------------------------------------------------------------------
double readPotiLR() {
  int analogVal = adc.getRawResult();

  double m = -0.0038;
  double b = 101.2;
  double y = m * analogVal + b;
  return y;
}
// zwischen 30 und 80 mm auf 1/10

//---------------------------------------------------------------------------------------
int onChangePoti() {
  int startVal = readPotiRaw();
  int currentVal = startVal;

  while (startVal == currentVal) {
    currentVal = readPotiRaw();
  }
  return currentVal;
}



//Potentiometer calibration ------------------------------------------------------------------------------

int menu = 0;
int samples;
int gauge[20];
int analog[20];
double measured[20];

void potiCalibration(){

  switch (menu)
  {
  case 0:
    Serial.println("Start calibration? y/n");
    while(menu == 0){
     if (Serial.available()) {
      char confirmByte = Serial.read();
      if (confirmByte == 'y') {
        // start
        Serial.println("Starting.. ");
        menu = 1;
      } else if (confirmByte == 'n') {
        Serial.println("Canceled calibration.");
        break;
      }
     }
    }
    break;
  case 1:
    Serial.print("Set Number of samples: ");
    while(menu == 1){
      if (Serial.available()) {
        samples = Serial.parseInt();
        //Serial.print("The Samplerate is: "); Serial.println(samples);
        menu = 2;
      }
    }
    break;
  case 2:
    Serial.println();
    Serial.print("Starting a set of  ");
    Serial.print(samples);
    Serial.println(" Samples.");
    while(menu == 2){

      for(int i=0; i < samples; i++){
        //gauge size definition
        Serial.print(i+1);
        Serial.print(". gauge size: ");
        while (Serial.available() == 0){} //waiting for data
        int readGauge = Serial.parseInt();
        gauge[i] = readGauge;
        Serial.println();

        //analog reading
        analog[i] = readPotiRawSingle();

        //measured distance
        //measured[i] = readPotiSingleShot();
      }
      menu = 3;
    }
    break;
  case 3:
    Serial.println("gauge | analog | measured");
    while(menu == 3){
      for(int i=0; i < samples; i++){
        Serial.print(gauge[i]);
        Serial.print(" | ");
        Serial.print(analog[i]);
        Serial.print(" | ");
        Serial.println(measured[i]);
      }
      menu = 0;
    }
    break;
  default:
    break;
  }
}