#include <Arduino.h>
#include <Wire.h>

#include "bme.h"
#include "communication.h"
#include "creep.h"
#include "loadcell.h"
#include "main.h"
#include "manual.h"
#include "motion.h"
#include "poti.h"
#include "relaxation.h"

// debugging
#define DEBUG
//#define MOTORTEST
#define DEBUGSTUFFF
//#define SERIALTEST

// activate
//#define PROGRAM
#define POTI
#define BME280
#define LOADCELL
#define MOTOR

// calibration
//#define CAL_POTI
//#define CAL_LOADCELL

#define EMERGENCY_STOP_BUTTON 27 //TEST
#define MOTOR_EN_PIN 17

struct parameter param;

bool homing_complete = false;

// TODO: Test other Power Source for Linear Poti
#ifdef DEBUGSTUFF
int rawMAX = 0;
int rawMIN = 30000;
double dmapMAX = -10.00;
double dmapMIN = 100.00;

// Timer for debugStuff
const unsigned int interval = 3000;
unsigned long startTimer = 0;
unsigned long endTimer = 0;

void debugStuff() {
#ifdef LOADCELL
  float weight = readloadcell();
  int intweight = readloadcell();
  float fastWeight = readloadcellfast();
  float force = getForce();
#endif
#ifdef POTI
  int potiraw = readPotiRaw();
  double dmapPoti = readPotiSingle();
  double potiLR = readPotiLR();
#endif
  /*
      Serial.print("weight: ");
      Serial.print(fastWeight);
      Serial.println(" g");
  */

  startTimer = millis();
  if (startTimer - endTimer > interval) {

#ifdef POTI
    Serial.print("RAW: ");
    Serial.print(potiraw);
    Serial.print(" | ");
    Serial.print("dmap: ");
    Serial.print(dmapPoti);
    Serial.print(" | ");
    Serial.print("Linear Regression: ");
    Serial.println(potiLR);

    Serial.println("\nMIN Raw | MAX Raw ");
    Serial.print(rawMIN);
    Serial.print(" | ");
    Serial.println(rawMAX);
    Serial.println("\nMIN dmap | MAX dmap ");
    Serial.print(dmapMIN);
    Serial.print(" | ");
    Serial.println(dmapMAX);
    Serial.println("--------------------------------");
#endif

#ifdef BME280
    Serial.print("Temperature: ");
    Serial.print(readTemp());
    Serial.print(" | ");
    Serial.print("Humidity: ");
    Serial.println(readHum());
    Serial.println("--------------------------------");
#endif

#ifdef LOADCELL
    Serial.print("weight: ");
    Serial.print(weight);
    Serial.print(" g");
    Serial.print(" | ");
    Serial.print("intweight: ");
    Serial.print((int)readloadcell());
    Serial.print(" g");
    Serial.print(" | ");
    Serial.print("force: ");
    Serial.print(force);
    Serial.println(" N");
    Serial.println("--------------------------------");
    // delay(1000);

#endif

    endTimer = startTimer;
  }
#ifdef POTI
  if (potiraw > rawMAX) {
    rawMAX = potiraw;
  }
  if (potiraw < rawMIN) {
    rawMIN = potiraw;
  }

  if (dmapPoti > dmapMAX) {
    dmapMAX = dmapPoti;
  }
  if (dmapPoti < dmapMIN) {
    dmapMIN = dmapPoti;
  }
#endif
}
#endif

void emergencyStop() {
#ifdef DEBUG
  Serial.println("Emergency Stop!");
#endif
  digitalWrite(MOTOR_EN_PIN, HIGH); // Disable Motor
}

void setup() {
  Serial.begin(115200);
  // Pi(TX)->ESP 9(RX) | Pi(RX)->ESP 10(TX)
  Serial1.begin(115200, SERIAL_8N1);

  // Interrupt for Emergency Button
  // LOW auf HIGH mit pulldown
  pinMode(EMERGENCY_STOP_BUTTON, INPUT);
  attachInterrupt(EMERGENCY_STOP_BUTTON, emergencyStop, RISING);

  // SCL: 22 | SDA: 21
  Wire.begin(); // Poti & BME280
  delay(10);

// init
#ifdef LOADCELL
  if (initLoadcell() == -1)
#endif
#ifdef DEBUG
    Serial.println("Load Cell connect failed");
#endif

#ifdef POTI
  if (initPoti() == -1)
#endif
#ifdef DEBUG
    Serial.println("Linear Potentiometer connect failed");
#endif

#ifdef BME280
  if (initBME280() == -1)
#endif
#ifdef DEBUG
    Serial.println("BME280 connect failed");
#endif

#ifdef MOTOR
  initMotor();
  delay(100);
#endif

}

void loop() {
//testingApproach();

#ifdef DEBUGSTUFF
  debugStuff();
#endif

#ifdef MOTORTEST
  testingMotor();
#endif

#ifdef SERIALTEST
//param = readSerial1Ready();
if (param.mode == 4){
  struct measurements mes;  
  /*
  mes.temperature = readTemp();                // Temperatur
  mes.humidity = readHum();                    // Luftfeuchtigkeit
  mes.distance = readPotiSingle() ;            //TEST Konpressionsdistanz
  mes.force = getForce();
  delay(10);
  */
  mes.mode = param.mode;
  mes.temperature = 22;                // Temperatur
  mes.humidity = 22;                    // Luftfeuchtigkeit
  mes.distance = 123 ;            //TEST Konpressionsdistanz
  mes.force = 12;
  //writeSerial(mes);
  
  writeSerial1(mes);

  delay(2000);
} else {
  param = readSerial1();
}
#endif

#if defined PROGRAM && !defined DEBUGSTUFF && !defined MOTORTEST
  if (!homing_complete) {
#ifdef DEBUG
    Serial.println("Start Homing");
#endif
    // start homing and check
    if (homing_endstopSwitch()) {
#ifdef DEBUG
      Serial.println("Homing complete");
#endif
      delay(100);
      homing_complete = 1;
    } else {
#ifdef DEBUG
      Serial.println("Homing not complete");
#endif
      delay(100);
      homing_complete = 0;
      // TODO: Error?
    }
  }
    // wait for serial data and read
#ifdef DEBUG
    Serial.println("IDLE...waiting for new Data");
#endif
    // blocks everything until data received
    // param = readSerial();

    // sending every 3s mode=0 -> ready for Data
    //param = readSerial1();

    param = readSerial1();
  

  switch (param.mode) {
  case 1: // Kriechversuch
    startCreepAttempt(param);
    // reset parameter
    param.mode = 0;
    param.duration = 0;
    param.distance = NAN;
    param.force = NAN;
    param.height = 0;
    param.mm = 0;
    // homing
    homing_complete = false;
    break;
  case 2: // Relaxationsversuch
    startRelaxationAttempt(param);
    // reset parameter
    param.mode = 0;
    param.duration = 0;
    param.distance = NAN;
    param.force = NAN;
    param.height = 0;
    param.mm = 0;
    homing_complete = false;
    break;
  case 3: // Manueller Betrieb
    manualMove(param);
    break;
  }
#endif
}